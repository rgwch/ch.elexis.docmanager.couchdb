/*******************************************************************************
 * Copyright (c) 2011, G. Weirich and Elexis
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    G. Weirich - initial implementation
 *    
 *    $Id: CouchContentProvider.java b283431087f6 2011/10/01 17:48:47 rgw $
 *******************************************************************************/

package ch.elexis.docmanager.couchdb.presenter;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import ch.elexis.docmanager.couchdb.model.CouchLoader;
import ch.elexis.docmanager.couchdb.model.Document;

public class CouchContentProvider implements ITreeContentProvider {
	CouchLoader loader;
	
	@Override
	public void dispose(){
	// TODO Auto-generated method stub
	
	}
	
	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput){
		loader = (CouchLoader) newInput;
	}
	
	@Override
	public Object[] getElements(Object inputElement){
		CouchLoader loader = (CouchLoader) inputElement;
		return loader.getCategories();
	}
	
	@Override
	public Object[] getChildren(Object parentElement){
		return loader.get((String) parentElement).toArray();
	}
	
	@Override
	public Object getParent(Object element){
		if (element instanceof String) {
			return loader;
		} else if (element instanceof Document) {
			return ((Document) element).getCategory();
		} else {
			return null;
		}
	}
	
	@Override
	public boolean hasChildren(Object element){
		if (element instanceof String && loader.get((String) element) != null) {
			return true;
		}
		return false;
	}
	
}
