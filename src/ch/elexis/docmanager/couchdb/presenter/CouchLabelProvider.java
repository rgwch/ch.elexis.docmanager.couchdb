/*******************************************************************************
 * Copyright (c) 2011, G. Weirich and Elexis
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    G. Weirich - initial implementation
 *    
 *    $Id: CouchLabelProvider.java b283431087f6 2011/10/01 17:48:47 rgw $
 *******************************************************************************/

package ch.elexis.docmanager.couchdb.presenter;

import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import ch.elexis.docmanager.couchdb.model.Document;

public class CouchLabelProvider extends LabelProvider implements ITableLabelProvider {
	
	public String getColumnText(Object obj, int index){
		if (obj instanceof String) {
			if (index == 1) {
				return (String) obj;
			}
			return ""; //$NON-NLS-1$
		}
		Document dh = (Document) obj;
		switch (index) {
		case 0:
		case 1:
			return ""; //$NON-NLS-1$
		case 2:
			return dh.getCreationDate();
		case 3:
			return dh.getTitle();
		case 4:
			return dh.getKeywords();
		default:
			return "?"; //$NON-NLS-1$
		}
	}
	
	public Image getColumnImage(Object obj, int index){
		return null; // getImage(obj);
	}
	
	public Image getImage(Object obj){
		return PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
	}
	
}
