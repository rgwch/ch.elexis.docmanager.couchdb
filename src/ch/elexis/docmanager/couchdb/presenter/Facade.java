/*******************************************************************************
 * Copyright (c) 2011, G. Weirich and Elexis
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    G. Weirich - initial implementation
 *    
 *     $Id: Facade.java b9eef05cd7f1 2012/02/14 09:59:34 marcode79 $
 *******************************************************************************/

package ch.elexis.docmanager.couchdb.presenter;

import java.io.File;
import java.io.FileOutputStream;
import java.text.MessageFormat;

import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.statushandlers.StatusManager;

import ch.elexis.Desk;
import ch.elexis.ElexisException;
import ch.elexis.Images;
import ch.elexis.core.ElexisCoreException;
import ch.elexis.data.Patient;
import ch.elexis.docmanager.couchdb.model.CouchLoader;
import ch.elexis.docmanager.couchdb.model.CouchLoader.LoaderCallback;
import ch.elexis.docmanager.couchdb.model.Document;
import ch.elexis.docmanager.couchdb.model.DocumentManager;
import ch.elexis.docmanager.couchdb.view.FileImportDialog;
import ch.elexis.text.IOpaqueDocument;
import ch.elexis.util.SWTHelper;
import ch.rgw.io.FileTool;
import ch.rgw.io.Settings;
import ch.rgw.tools.ExHandler;
import ch.rgw.tools.MimeTool;

/**
 * This is the presenter layer of the plugin that connects the model with a view. It defines and
 * populates the viewer and offers some methods for configuration purposes.
 * 
 * @author gerry
 * 
 */
public class Facade implements LoaderCallback {
	public static final String PLUGIN_ID = "ch.elexis.docmanager.couchdb";
	private static final String PREFBASE = "plugins/docmgr_couchdb/"; //$NON-NLS-1$
	private static final String PREF_DBURL = PREFBASE + "db_url"; //$NON-NLS-1$
	private static final String PREF_DBNAME = PREFBASE + "db_name"; //$NON-NLS-1$
	private static final String PREF_KEY = PREFBASE + "db_key"; //$NON-NLS-1$
	private static final String PREF_ENCMODE = PREFBASE + "db_enc"; //$NON-NLS-1$
	
	private final String[] colLabels =
		{
			"", Messages.Facade_labelCategory, Messages.Facade_labelDate, Messages.Facade_labelTitle, Messages.Facade_labelKeywords //$NON-NLS-1$
		};
	private final int[] colWidth = {
		20, 80, 80, 150, 500
	};
	private Action importAction, editAction, deleteAction, scanAction, exportAction,
			doubleClickAction;
	
	private TreeViewer tv;
	private DocumentManager dm;
	private CouchLoader loader;
	private static Settings cfg;
	
	public static void setCfg(Settings cfg){
		Facade.cfg = cfg;
	}
	
	/**
	 * get the preference value for "store encrypted"
	 * 
	 * @return true if data is stored encrypted
	 */
	public static boolean getEncmode(){
		return cfg == null ? false : cfg.get(PREF_ENCMODE, false);
	}
	
	/**
	 * set the preference for "store encrypted"
	 * 
	 * @param bMode
	 *            true id documents should be stored encrypted
	 */
	public static void setEncmode(boolean bMode){
		cfg.set(PREF_ENCMODE, bMode);
	}
	
	/**
	 * Get the key used for encryption
	 * 
	 * @return a base64 encoded blowfish key
	 */
	public static String getKey(){
		return cfg.get(PREF_KEY, null);
	}
	
	/**
	 * Set the key to use for enryption
	 * 
	 * @param key
	 *            a base64 encoded blowfish key
	 */
	public static void setKey(String key){
		cfg.set(PREF_KEY, key);
	}
	
	/**
	 * Get the URL of the CouchDB Server to use
	 * 
	 * @return an url, may be "localhost" if none is defined
	 */
	public static String getDBUrl(){
		return cfg.get(PREF_DBURL, "localhost"); //$NON-NLS-1$
	}
	
	/**
	 * Get the name of the CouchDB Database to use
	 * 
	 * @return a database name
	 */
	public static String getDBName(){
		return cfg.get(PREF_DBNAME, "elexisdox"); //$NON-NLS-1$
	}
	
	/**
	 * Set the URL of the CouchDB Server to use
	 * 
	 * @param url
	 *            a name or IP, without protokol prefix
	 */
	public static void setDBUrl(String url){
		cfg.set(PREF_DBURL, url);
		
	}
	
	/**
	 * Set the name of the CouchDB database to use or to create
	 * 
	 * @param name
	 *            a valid CouchDB database name. If no such database exists on the specified server,
	 *            it will be created on first access
	 */
	public static void setDBName(String name){
		cfg.set(PREF_DBNAME, name);
	}
	
	/**
	 * Create the viewer for database objects. 3rd party software should not call this method
	 * 
	 * @param parent
	 * @return
	 * @throws ElexisCoreException
	 */
	public StructuredViewer createViewer(Composite parent) throws ElexisCoreException{
		dm = new DocumentManager();
		loader = new CouchLoader(dm);
		createTree(parent);
		tv.setContentProvider(new CouchContentProvider());
		tv.setLabelProvider(new CouchLabelProvider());
		tv.setUseHashlookup(true);
		hookDropSupport();
		hookDoubleClickAction();
		tv.setInput(loader);
		return tv;
	}
	
	/**
	 * Change the Patient to display. This will trigger a reload from the database to retrieve this
	 * patient's documents.
	 * 
	 * @param pat
	 *            The Patient to set
	 * @throws ElexisException
	 */
	public void setPatient(Patient pat) throws ElexisException{
		loader.reload(pat==null ? null : pat.getId(), this);
	}
	
	private void createTree(Composite parent){
		tv = new TreeViewer(parent, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION);
		Tree tree = tv.getTree();
		TreeColumn[] cols = new TreeColumn[colLabels.length];
		CouchSorter sorter = new CouchSorter(tv);
		makeActions();
		for (int i = 0; i < colLabels.length; i++) {
			cols[i] = new TreeColumn(tree, SWT.NONE);
			cols[i].setWidth(colWidth[i]);
			cols[i].setText(colLabels[i]);
			cols[i].setData(new Integer(i));
			cols[i].addSelectionListener(sorter.createListener());
		}
		tree.setHeaderVisible(true);
		tree.setLinesVisible(true);
		tv.setSorter(sorter);
	}
	
	private void hookDropSupport(){
		final Transfer[] transferTypes = new Transfer[] {
			FileTransfer.getInstance()
		};
		tv.addDropSupport(DND.DROP_COPY, transferTypes, new DropTargetAdapter() {
			
			@Override
			public void dragEnter(DropTargetEvent event){
				event.detail = DND.DROP_COPY;
			}
			
			@Override
			public void drop(DropTargetEvent event){
				if (transferTypes[0].isSupportedType(event.currentDataType)) {
					String[] files = (String[]) event.data;
					for (String filename : files) {
						try {
							new FileImportDialog(dm, filename).open();
						} catch (ElexisException ex) {
							StatusManager.getManager().handle(
								new Status(Status.WARNING, PLUGIN_ID,
									"Error while importing files ", ex),
								StatusManager.LOG | StatusManager.SHOW);
						}
					}
				}
				
			}
			
		});
	}
	
	private void hookDoubleClickAction(){
		tv.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event){
				doubleClickAction.run();
			}
		});
	}
	
	/**
	 * Create a context menu for CouchDB documents
	 * 
	 * @param site
	 */
	public void createContextMenu(IWorkbenchPartSite site){
		
		MenuManager menuMgr = new MenuManager("#PopupMenu"); //$NON-NLS-1$
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager){
				manager.add(editAction);
				manager.add(deleteAction);
				manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
				
			}
		});
		Menu menu = menuMgr.createContextMenu(tv.getControl());
		tv.getControl().setMenu(menu);
		site.registerContextMenu(menuMgr, tv);
	}
	
	/**
	 * Add menus for CouchDB documents to the view menu and toolbar
	 * 
	 * @param bars
	 */
	public void contributeToActionBars(IActionBars bars){
		// local pull down
		IMenuManager pd = bars.getMenuManager();
		pd.add(importAction);
		pd.add(exportAction);
		pd.add(editAction);
		
		IToolBarManager tb = bars.getToolBarManager();
		tb.add(importAction);
		tb.add(exportAction);
		// tb.add(scanAction);
	}
	
	private void makeActions(){
		
		importAction = new Action("import") {
			{
				setToolTipText("import a document from disk");
				setImageDescriptor(Images.IMG_IMPORT.getImageDescriptor());
			}
			
			public void run(){
				FileDialog fd = new FileDialog(tv.getControl().getShell(), SWT.OPEN);
				String filename = fd.open();
				if (filename != null) {
					try {
						new FileImportDialog(dm, filename).open();
					} catch (ElexisException e) {
						SWTHelper.showError("error importing document", e.getMessage());
					}
				}
			}
		};
		
		deleteAction = new Action("delete") {
			{
				setToolTipText("delete a document");
				setImageDescriptor(Images.IMG_DELETE.getImageDescriptor());
			}
			
			public void run(){
				ISelection selection = tv.getSelection();
				Object obj = ((IStructuredSelection) selection).getFirstElement();
				Document dh = (Document) obj;
				
				if (SWTHelper.askYesNo("confirm delete", MessageFormat.format(
					"Do you really want to delete {0} permanently?", dh.getTitle()))) {
					dm.removeDocument(dh.getId());
					// ElexisEventDispatcher.getInstance().fire(new
					// ElexisEvent(dh,Document.class,ElexisEvent.EVENT_DELETE));
				}
			}
		};
		editAction = new Action("edit") {
			{
				setToolTipText("Edit document metadata");
				setImageDescriptor(Images.IMG_EDIT.getImageDescriptor());
			}
			
			public void run(){
				try {
					ISelection selection = tv.getSelection();
					Document dh = (Document) ((IStructuredSelection) selection).getFirstElement();
					IOpaqueDocument doc = Document.pull(dh);
					FileImportDialog fid = new FileImportDialog(doc);
					if (fid.open() == Dialog.OK) {
						tv.refresh(true);
					}
				} catch (ElexisException e) {
					SWTHelper.alert("Error retrieving document", e.getMessage());
				}
				
			}
		};
		
		doubleClickAction = new Action() {
			public void run(){
				ISelection selection = tv.getSelection();
				Object obj = ((IStructuredSelection) selection).getFirstElement();
				Document dh = (Document) obj;
				execute(dh);
				
			}
		};
		
		exportAction = new Action("export") {
			{
				setImageDescriptor(Images.IMG_EXPORT.getImageDescriptor());
				setToolTipText("export document to disk");
			}
			
			public void run(){
				ISelection selection = tv.getSelection();
				Object obj = ((IStructuredSelection) selection).getFirstElement();
				IOpaqueDocument dh;
				try {
					dh = Document.pull((Document) obj);
					String name = dh.getTitle();
					FileDialog fd = new FileDialog(tv.getControl().getShell(), SWT.SAVE);
					fd.setFileName(name);
					String fname = fd.open();
					if (fname != null) {
						FileOutputStream fos = new FileOutputStream(fname);
						FileTool.copyStreams(dh.getContentsAsStream(), fos);
						fos.close();
					}
					
				} catch (Exception e) {
					SWTHelper.showError("Export error", e.getMessage());
				}
				
			}
		};
	}
	
	/**
	 * Try to display a document contents with the Program that is configured in the system for this
	 * mimetype.
	 * 
	 * @param doc
	 */
	public void execute(Document dr){
		try {
			IOpaqueDocument doc = Document.pull(dr);
			String typname = doc.getMimeType();
			String ext = MimeTool.getExtension(typname);
			if (ext.length() == 0) { // No matching extension for this mimetype
				// found. Try extension
				int r = typname.lastIndexOf('.');
				if (r == -1) {
					typname = doc.getTitle();
					r = typname.lastIndexOf('.');
				}
				
				if (r != -1) {
					ext = typname.substring(r + 1);
				}
			}
			File temp = File.createTempFile("omni_", "_vore." + ext); //$NON-NLS-1$ //$NON-NLS-2$
			temp.deleteOnExit();
			byte[] b = doc.getContentsAsBytes();
			if (b == null) {
				SWTHelper.showError("cant read", "error");
				return;
			}
			FileOutputStream fos = new FileOutputStream(temp);
			fos.write(b);
			fos.close();
			Program proggie = Program.findProgram(ext);
			if (proggie != null) {
				proggie.execute(temp.getAbsolutePath());
			} else {
				if (Program.launch(temp.getAbsolutePath()) == false) {
					Runtime.getRuntime().exec(temp.getAbsolutePath());
				}
				
			}
			
		} catch (Exception ex) {
			ExHandler.handle(ex);
			SWTHelper.showError("error", ex.getMessage());
		}
	}
	
	@Override
	public void reloaded(){
		if (tv != null) {
			Desk.asyncExec(new Runnable() {
				
				@Override
				public void run(){
					tv.refresh();
				}
			});
		}
	}
	
}
