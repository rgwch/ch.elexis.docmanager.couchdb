package ch.elexis.docmanager.couchdb.presenter;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "ch.elexis.docmanager.couchdb.presenter.messages"; //$NON-NLS-1$
	public static String Facade_labelCategory;
	public static String Facade_labelDate;
	public static String Facade_labelKeywords;
	public static String Facade_labelTitle;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}
	
	private Messages(){}
}
