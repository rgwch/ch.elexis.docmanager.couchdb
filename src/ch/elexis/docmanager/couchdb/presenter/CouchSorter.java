/*******************************************************************************
 * Copyright (c) 2011, G. Weirich and Elexis
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    G. Weirich - initial implementation
 *    
 *    $Id: CouchSorter.java 2dc4165d178a 2012/04/07 05:54:10 rgw $
 *******************************************************************************/

package ch.elexis.docmanager.couchdb.presenter;

import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.TreeColumn;

import ch.elexis.core.Session;
import ch.elexis.docmanager.couchdb.model.Document;
import ch.rgw.tools.TimeTool;

/**
 * Sorter for CouchDB objects
 * 
 * @author gerry
 * 
 */
public class CouchSorter extends ViewerSorter {
	private int sortMode = SORTMODE_DATE;
	private boolean bReverse = false;
	private StructuredViewer viewer;
	public static final int SORTMODE_DATE = 0;
	public static final int SORTMODE_TITLE = 1;
	
	private static final String SORTMODE_DEF = "couchdb/sortmode"; //$NON-NLS-1$
	
	public CouchSorter(StructuredViewer sv){
		viewer = sv;
	}
	
	public SortListener createListener(){
		return new SortListener();
	}
	
	@Override
	public int compare(Viewer viewer, Object e1, Object e2){
		if ((e1 instanceof Document) && (e2 instanceof Document)) {
			Document d1 = (Document) e1;
			Document d2 = (Document) e2;
			String c1, c2;
			if (sortMode == SORTMODE_DATE) {
				c1 = new TimeTool(d1.getCreationDate()).toString(TimeTool.DATE_COMPACT);
				c2 = new TimeTool(d2.getCreationDate()).toString(TimeTool.DATE_COMPACT);
			} else if (sortMode == SORTMODE_TITLE) {
				c1 = d1.getTitle().toLowerCase();
				c2 = d2.getTitle().toLowerCase();
			} else {
				c1 = ""; //$NON-NLS-1$
				c2 = ""; //$NON-NLS-1$
			}
			if (bReverse) {
				return c1.compareTo(c2);
			} else {
				return c2.compareTo(c1);
			}
		}
		return 0;
	}
	
	public void setSortmode(int sm){
		sortMode = sm;
	}
	
	public void setReverse(boolean r){
		bReverse = r;
	}
	
	public void setUser(){
		String[] defsort = Session.getInstance().getUserCfg().get(SORTMODE_DEF, "0,1").split(","); //$NON-NLS-1$ //$NON-NLS-2$
		try {
			sortMode = Integer.parseInt(defsort[0]);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		bReverse = defsort.length > 1 ? defsort[1].equals("1") : false; //$NON-NLS-1$
		
	}
	
	class SortListener extends SelectionAdapter {
		
		@Override
		public void widgetSelected(SelectionEvent e){
			TreeColumn col = (TreeColumn) e.getSource();
			
			if (col.getData().equals(2)) {
				if (sortMode == SORTMODE_DATE) {
					bReverse = !bReverse;
				}
				sortMode = SORTMODE_DATE;
			} else if (col.getData().equals(3)) {
				if (sortMode == SORTMODE_TITLE) {
					bReverse = !bReverse;
				}
				sortMode = SORTMODE_TITLE;
			}
			Session.getInstance().getUserCfg()
				.set(SORTMODE_DEF, Integer.toString(sortMode) + "," + (bReverse ? "1" : "0")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			viewer.refresh();
		}
	}
	
}
