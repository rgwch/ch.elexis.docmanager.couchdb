/*******************************************************************************
 * Copyright (c) 2011, G. Weirich and Elexis
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    G. Weirich - initial implementation
 *    
 *    $Id: CouchDBView.java eadab47e9d03 2012/04/28 15:45:55 rgw $
 *******************************************************************************/

package ch.elexis.docmanager.couchdb.view;

import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

import ch.elexis.ElexisException;
import ch.elexis.actions.GlobalEventDispatcher;
import ch.elexis.actions.GlobalEventDispatcher.IActivationListener;
import ch.elexis.core.ElexisCoreException;
import ch.elexis.core.Session;
import ch.elexis.core.events.ElexisEvent;
import ch.elexis.core.events.ElexisEventDispatcher;
import ch.elexis.core.events.ElexisEventListenerImpl;
import ch.elexis.data.Anwender;
import ch.elexis.data.Patient;
import ch.elexis.docmanager.couchdb.model.Document;
import ch.elexis.docmanager.couchdb.presenter.Facade;
import ch.elexis.util.SWTHelper;

public class CouchDBView extends ViewPart implements IActivationListener {
	
	private Facade facade;
	private StructuredViewer viewer;
	
	private final ElexisEventListenerImpl eeli_pat =
		new ElexisEventListenerImpl(Patient.class, ElexisEvent.EVENT_SELECTED) {
			@Override
			public void runInUi(ElexisEvent ev){
				setPatient(ev.getObject());
			}
			
		};
	
	private final ElexisEventListenerImpl eeli_user =
		new ElexisEventListenerImpl(Anwender.class, ElexisEvent.EVENT_USER_CHANGED) {
			@Override
			public void runInUi(ElexisEvent ev){
				setPatient(ElexisEventDispatcher.getSelectedPatient());
			}
		};
	
	private final ElexisEventListenerImpl eeli_doc =
		new ElexisEventListenerImpl(Document.class, ElexisEvent.EVENT_CREATE
			| ElexisEvent.EVENT_DELETE) {
			@Override
			public void runInUi(ElexisEvent ev){
				setPatient(ElexisEventDispatcher.getSelectedPatient());
			}
			
		};
	
	public CouchDBView(){}
	
	public void createPartControl(Composite parent){
		facade = new Facade();
		Facade.setCfg(Session.getInstance().getGlobalCfg());
		try {
			viewer = facade.createViewer(parent);
			viewer.getControl().setLayoutData(SWTHelper.getFillGridData(1, true, 1, true));
		} catch (ElexisCoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		facade.createContextMenu(getSite());
		facade.contributeToActionBars(getViewSite().getActionBars());
		GlobalEventDispatcher.addActivationListener(this, this);
		eeli_user.catchElexisEvent(ElexisEvent.createUserEvent());
		
	}
	
	@Override
	public void dispose(){
		GlobalEventDispatcher.removeActivationListener(this, this);
		super.dispose();
	}
	
	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus(){
		viewer.getControl().setFocus();
	}
	
	public void activation(boolean mode){

	}
	
	void setPatient(Object pat){
		try {
			facade.setPatient((Patient) pat);
		} catch (ElexisException e) {
			SWTHelper.alert(Messages.CouchDBView_errorLoadingCouchDB, e.getMessage());
			
		}
		
	}
	
	public void visible(boolean mode){
		if (mode) {
			ElexisEventDispatcher.getInstance().addListeners(eeli_pat, eeli_user, eeli_doc);
			setPatient(ElexisEventDispatcher.getSelectedPatient());
		} else {
			ElexisEventDispatcher.getInstance().removeListeners(eeli_pat, eeli_user, eeli_doc);
		}
		
	}
	
}
