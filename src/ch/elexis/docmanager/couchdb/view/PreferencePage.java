/*******************************************************************************
 * Copyright (c) 2011, G. Weirich and Elexis
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    G. Weirich - initial implementation
 *    
 *    $Id: PreferencePage.java 2dc4165d178a 2012/04/07 05:54:10 rgw $
 *******************************************************************************/

package ch.elexis.docmanager.couchdb.view;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import ch.elexis.core.Session;
import ch.elexis.docmanager.couchdb.presenter.Facade;
import ch.elexis.preferences.SettingsPreferenceStore;
import ch.rgw.crypt.Base64Coder;
import ch.rgw.crypt.JCECrypter;
import ch.rgw.io.InMemorySettings;
import ch.rgw.tools.StringTool;

public class PreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	
	public static final String PREFERENCE_BASE = "plugins/docmgr_couchdb"; //$NON-NLS-1$
	InMemorySettings cfg = new InMemorySettings();
	
	public PreferencePage(){
		super(GRID);
		setPreferenceStore(new SettingsPreferenceStore(cfg));
	}
	
	@Override
	public void init(IWorkbench workbench){
		Facade.setCfg(Session.getInstance().getGlobalCfg());
		cfg.set("url", Facade.getDBUrl()); //$NON-NLS-1$
		cfg.set("name", Facade.getDBName()); //$NON-NLS-1$
		cfg.set("enc", Facade.getEncmode()); //$NON-NLS-1$
		String key = Facade.getKey();
		if (StringTool.isNothing(key)) {
			byte[] k = new JCECrypter().generateBlowfishKey();
			key = new String(Base64Coder.encode(k));
			Facade.setKey(key);
			Session.getInstance().getGlobalCfg().flush();
		}
		cfg.set("key", Facade.getKey()); //$NON-NLS-1$
	}
	
	@Override
	protected void createFieldEditors(){
		addField(new StringFieldEditor(
			"url", Messages.PreferencePage_lblServerUrl, getFieldEditorParent())); //$NON-NLS-1$
		addField(new StringFieldEditor(
			"name", Messages.PreferencePage_lblDatabaseName, getFieldEditorParent())); //$NON-NLS-1$
		addField(new BooleanFieldEditor(
			"enc", Messages.PreferencePage_lblStoreEncrypted, getFieldEditorParent())); //$NON-NLS-1$
		//addField(new MultilineFieldEditor("key", Messages.PreferencePage_lblKey, getFieldEditorParent())); //$NON-NLS-1$
	}
	
	@Override
	protected void performApply(){
		super.performApply();
		Facade.setDBName(cfg.get("name", "")); //$NON-NLS-1$ //$NON-NLS-2$
		Facade.setDBUrl(cfg.get("url", "")); //$NON-NLS-1$ //$NON-NLS-2$
		Facade.setEncmode(cfg.get("enc", false)); //$NON-NLS-1$
		Session.getInstance().getGlobalCfg().flush();
	}
	
}
