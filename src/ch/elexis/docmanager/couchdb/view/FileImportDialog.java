/*******************************************************************************
 * Copyright (c) 2006-2011, G. Weirich and Elexis
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    G. Weirich - initial implementation
 *    
 *  $Id: FileImportDialog.java eadab47e9d03 2012/04/28 15:45:55 rgw $
 *******************************************************************************/

package ch.elexis.docmanager.couchdb.view;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ch.elexis.ElexisException;
import ch.elexis.Hub;
import ch.elexis.Images;
import ch.elexis.core.events.ElexisEventDispatcher;
import ch.elexis.data.Patient;
import ch.elexis.docmanager.couchdb.model.Document;
import ch.elexis.docmanager.couchdb.model.DocumentManager;
import ch.elexis.text.IOpaqueDocument;
import ch.elexis.util.SWTHelper;
import ch.rgw.io.FileTool;
import ch.rgw.tools.MimeTool;
import ch.rgw.tools.TimeTool;

public class FileImportDialog extends TitleAreaDialog {
	DocumentManager dm;
	Document doc;
	Text tTitle;
	Text tKeywords;
	Combo cbCategories;
	
	public FileImportDialog(DocumentManager dm, String filename) throws ElexisException{
		super(Hub.getPlugin().getWorkbench().getActiveWorkbenchWindow().getShell());
		try {
			this.dm = dm;
			doc = new Document(dm);
			Patient act = ElexisEventDispatcher.getSelectedPatient();
			if (act != null) {
				doc.setPatientID(act.getId());
			}
			File file = new File(filename);
			doc.setTitle(file.getName());
			String ext = FileTool.getExtension(filename);
			doc.setMimeType(MimeTool.getMimeType(ext));
			FileInputStream fis = new FileInputStream(file);
			ByteArrayOutputStream baos = new ByteArrayOutputStream((int) file.length());
			FileTool.copyStreams(fis, baos);
			fis.close();
			baos.close();
			doc.setContents(baos.toByteArray());
			TimeTool tt = new TimeTool(file.lastModified());
			doc.setCreationDate(tt.toString(TimeTool.FULL_ISO));
		} catch (FileNotFoundException e) {
			throw new ElexisException(getClass(), "File not found: " + filename,
				ElexisException.EE_NOT_FOUND);
		} catch (IOException e) {
			throw new ElexisException(getClass(), "IO Error:  " + e.getMessage(),
				ElexisException.EE_FILE_ERROR);
		} 
	}
	
	public FileImportDialog(IOpaqueDocument dr){
		super(Hub.getPlugin().getWorkbench().getActiveWorkbenchWindow().getShell());
		this.doc = (Document) dr;
		this.dm = doc.getDocumentManager();
	}
	
	@Override
	protected Control createDialogArea(Composite parent){
		Composite ret = new Composite(parent, SWT.NONE);
		ret.setLayout(new GridLayout());
		ret.setLayoutData(SWTHelper.getFillGridData(1, true, 1, true));
		new Label(ret, SWT.None).setText(Messages.FileImportDialog_labelCategory);
		Composite cCats = new Composite(ret, SWT.NONE);
		cCats.setLayoutData(SWTHelper.getFillGridData(1, true, 1, false));
		// RowLayout rl=new RowLayout(SWT.HORIZONTAL);
		// rl.fill=true;
		
		cCats.setLayout(new GridLayout(4, false));
		cbCategories = new Combo(cCats, SWT.SINGLE | SWT.DROP_DOWN | SWT.READ_ONLY);
		cbCategories.setLayoutData(SWTHelper.getFillGridData());
		Button bNewCat = new Button(cCats, SWT.PUSH);
		bNewCat.setImage(Images.IMG_NEW.getImage());
		bNewCat.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e){
				InputDialog id =
					new InputDialog(getShell(), Messages.FileImportDialog_captionNewCategory,
						Messages.FileImportDialog_textNewCategory, null, null);
				if (id.open() == Dialog.OK) {
					dm.addCategory(id.getValue());
					cbCategories.add(id.getValue());
					cbCategories.setText(id.getValue());
				}
			}
		});
		Button bEditCat = new Button(cCats, SWT.PUSH);
		bEditCat.setImage(Images.IMG_EDIT.getImage());
		bEditCat.setToolTipText(Messages.FileImportDialog_captionRenameCategory);
		bEditCat.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e){
				String old = cbCategories.getText();
				InputDialog id =
					new InputDialog(getShell(), MessageFormat.format(
						Messages.FileImportDialog_captionDlgRenameCateory, old),
						Messages.FileImportDialog_textDlgRenameCategory, old, null);
				if (id.open() == Dialog.OK) {
					String nn = id.getValue();
					dm.renameCategory(old, nn);
					cbCategories.remove(old);
					cbCategories.add(nn);
				}
			}
		});
		
		Button bDeleteCat = new Button(cCats, SWT.PUSH);
		bDeleteCat.setImage(Images.IMG_DELETE.getImage());
		bDeleteCat.setToolTipText(Messages.FileImportDialog_ttdeleteCategory);
		bDeleteCat.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent ev){
				String old = cbCategories.getText();
				InputDialog id =
					new InputDialog(getShell(), MessageFormat.format(
						Messages.FileImportDialog_captionDlgDeleteCategory, old),
						Messages.FileImportDialog_textDlgDeleteCategory, "", null); //$NON-NLS-2$
				if (id.open() == Dialog.OK) {
					dm.removeCategory(old, id.getValue());
					cbCategories.remove(id.getValue());
				}
			}
		});
		List<String> cats = Arrays.asList(dm.getCategories());
		if (cats.size() > 0) {
			cbCategories.setItems(cats.toArray(new String[0]));
			cbCategories.select(0);
		}
		new Label(ret, SWT.NONE).setText(Messages.FileImportDialog_labelTitle);
		tTitle = SWTHelper.createText(ret, 1, SWT.NONE);
		new Label(ret, SWT.NONE).setText(Messages.FileImportDialog_labelKeywords);
		tKeywords = SWTHelper.createText(ret, 4, SWT.NONE);
		tTitle.setText(doc.getTitle());
		if (doc != null) {
			tKeywords.setText(doc.getKeywords());
			cbCategories.setText(doc.getCategory());
		}
		return ret;
	}
	
	@Override
	public void create(){
		super.create();
		setTitle(doc.getTitle());
		getShell().setText(Messages.FileImportDialog_captionImportFile);
		setMessage(Messages.FileImportDialog_textImportFile);
	}
	
	@Override
	protected void okPressed(){
		doc.setKeywords(tKeywords.getText());
		doc.setTitle(tTitle.getText());
		doc.setCategory(cbCategories.getText());
		try {
			doc.push();
			
		} catch (ElexisException e) {
			SWTHelper.alert("Could not store document", e.getMessage());
		}
		super.okPressed();
	}
	
}
