package ch.elexis.docmanager.couchdb.view;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "ch.elexis.docmanager.couchdb.view.messages"; //$NON-NLS-1$
	public static String CouchDBView_errorLoadingCouchDB;
	public static String FileImportDialog_captionDlgDeleteCategory;
	public static String FileImportDialog_captionDlgRenameCateory;
	public static String FileImportDialog_captionImportFile;
	public static String FileImportDialog_captionNewCategory;
	public static String FileImportDialog_captionRenameCategory;
	public static String FileImportDialog_labelCategory;
	public static String FileImportDialog_labelKeywords;
	public static String FileImportDialog_labelTitle;
	public static String FileImportDialog_textDlgDeleteCategory;
	public static String FileImportDialog_textDlgRenameCategory;
	public static String FileImportDialog_textImportFile;
	public static String FileImportDialog_textNewCategory;
	public static String FileImportDialog_ttdeleteCategory;
	public static String PreferencePage_lblDatabaseName;
	public static String PreferencePage_lblKey;
	public static String PreferencePage_lblServerUrl;
	public static String PreferencePage_lblStoreEncrypted;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}
	
	private Messages(){}
}
