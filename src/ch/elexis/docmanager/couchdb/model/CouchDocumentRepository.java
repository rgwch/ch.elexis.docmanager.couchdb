package ch.elexis.docmanager.couchdb.model;

import java.util.List;

import org.ektorp.CouchDbConnector;
import org.ektorp.support.CouchDbRepositorySupport;
import org.ektorp.support.GenerateView;
import org.ektorp.support.View;

@View( name="all", map="function(doc){ if(doc.patientID){" + " emit(doc.patientID, {title: doc.title,"
			+ "keywords: doc.keywords," + "patientID: doc.patientID," + "category: doc.category,"
			+ "creationDate: doc.creationDate," + "date: doc.date" + " });" + "}}")
public class CouchDocumentRepository extends CouchDbRepositorySupport<Document> {
	
	public CouchDocumentRepository(CouchDbConnector db){
		super(Document.class,db);
		initStandardDesignDocument();
	}
	
	@GenerateView
	public List<Document> findByPatientID(String patid){
		return queryView("by_patientID",patid);
	}
}
