/*******************************************************************************
 * Copyright (c) 2011, G. Weirich and Elexis
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    G. Weirich - initial implementation
 *******************************************************************************/

package ch.elexis.docmanager.couchdb.model;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.ektorp.DocumentNotFoundException;

import ch.elexis.ElexisException;
import ch.elexis.core.data.ISelectable;
import ch.elexis.docmanager.couchdb.presenter.Facade;
import ch.elexis.text.IOpaqueDocument;
import ch.rgw.crypt.Base64Coder;
import ch.rgw.crypt.CryptologistException;
import ch.rgw.tools.MimeTool;
import ch.rgw.tools.StringTool;

public class Document implements IOpaqueDocument, ISelectable {
	private static final String ATTACHMENT_HEADER = "ctxi";

	private static final long serialVersionUID = 5864872225723034254L;

	public static final String NAME_CONTENTS = "contents"; //$NON-NLS-1$
	private String id;
	private String rev;
	private String title;
	private String mimeType;
	private String keywords;
	private String category;
	private String date;
	private String patientID;
	private boolean encrypted;
	boolean isProxy = true;
	private byte[] contents;
	private boolean bDirty;

	DocumentManager dm;

	@JsonProperty("_id")
	public String getId() {
		return id;
	}

	@JsonProperty("_id")
	public void setId(String id) {
		this.id = id;
	}

	@JsonProperty("_rev")
	public String getRev() {
		return rev;
	}

	@JsonProperty("_rev")
	public void setRev(String rev) {
		this.rev = rev;
	}

	@Override
	public String getCreationDate() {
		return date == null ? "?" : date;
	}

	public void setCreationDate(String date) {
		this.date = date;
	}

	@JsonIgnore
	public DocumentManager getDocumentManager() {
		return dm;
	}

	@JsonIgnore
	public void setDocumentManager(DocumentManager dm) {
		this.dm = dm;
	}

	public Document() {
	}

	public Document(DocumentManager dm) {
		this.dm = dm;
	}

	public Document(DocumentManager dm, String title, String id) {
		this(dm);
		this.title = title;
		setId(id == null ? StringTool.unique("couch") : id); //$NON-NLS-1$
	}

	public Document(DocumentManager dm, String title, String id,
			String patientID, byte[] contents) throws CryptologistException {
		this(dm, title, id);
		setPatientID(patientID);
		setContents(contents);
	}

	@Override
	public String getTitle() {
		return title == null ? "" : title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String getMimeType() {
		return mimeType == null ? MimeTool.BINARY : mimeType;
	}

	public void setMimeType(String mt) {
		mimeType = mt;
	}

	/**
	 * Add the "contents" attachment to the document. If encryption is seleted
	 * ion the preferences, the contents will be encrypted before attaching. If
	 * the contents is encrypted, its announced mimetype will always be
	 * "application/octet-stream". The key ist stored in the local elexis
	 * database.
	 * 
	 * @param cnt
	 *            the contents to attach. if it is null or its lenght is 0,
	 *            nothing will happen.
	 */
	@JsonIgnore
	public void setContents(final byte[] cnt) {
		contents = cnt;
		bDirty = true;
	}

	@Override
	@JsonIgnore
	public InputStream getContentsAsStream() throws ElexisException {
		byte[] att = getContentsAsBytes();
		ByteArrayInputStream bais = new ByteArrayInputStream(att);
		return bais;
	}

	/**
	 * retrieve the "contents" Attachment of the Document. Note that JCouchDb
	 * does not download attachments directly when fetching a document. for
	 * performance reasons, the application must fetch attachments separately as
	 * needed. This method will fetch the attachment when called for the first
	 * time on a given document, but will keep it cached then. If the attachment
	 * was encrypted, it will be decrypted using the key stored in the database
	 * 
	 */
	@Override
	@JsonIgnore
	public byte[] getContentsAsBytes() throws ElexisException {
		if (contents == null) {
			contents = loadContents();
		}
		return contents;

	}

	private byte[] loadContents() throws ElexisException {
		try {
			contents = null;
			Map<String, Object> data = dm.getDb().get(Map.class,
					ATTACHMENT_HEADER + getId());
			byte[] cnt = Base64Coder.decode((String)data.get("payload"));
			if (isEncrypted()) {
				contents = dm.decrypt(cnt);
			} else {
				contents = cnt;
			}
			return contents;

		} catch (DocumentNotFoundException nfe) {
			contents = null;
			return null;
		} catch (Exception ex) {
			contents = null;
			throw new ElexisException(
					getClass(),
					Messages.Document_errorRetrievingContents + ex.getMessage(),
					1);
		}

	}

	@Override
	public String getKeywords() {
		return keywords == null ? "" : keywords; //$NON-NLS-1$
	}

	public void setKeywords(String kw) {
		keywords = kw;
	}

	@Override
	public String getCategory() {
		return category == null ? "" : category;
	}

	public void setCategory(String cat) {
		category = cat;
	}

	@Override
	public String getPatientID() {
		return patientID == null ? "--" : patientID;
	}

	public void setPatientID(String id) {
		patientID = id == null ? "-" : id; //$NON-NLS-1$
	}

	@Override
	@JsonIgnore
	public String getGUID() {
		return getId();
	}

	public boolean isEncrypted() {
		return encrypted;
	}

	public void setEncrypted(boolean encrypted) {
		this.encrypted = encrypted;
	}

	public void push() throws ElexisException {
		setPatientID(dm.encryptString(patientID));
		if (getRev() == null) {
			dm.cdr.add(this);
		}
		if (contents != null && contents.length > 0 && bDirty) {
			byte[] cnt;
			if (Facade.getEncmode()) {
				try {
					cnt = dm.encrypt(contents);
				} catch (CryptologistException e) {
					throw new ElexisException(getClass(), e.getMessage(),
							e.getCode());
				}
				setEncrypted(true);
			} else {
				setEncrypted(false);
				cnt = contents;
			}
			Map<String, Object> data = null;
			try {
				data = dm.getDb().get(Map.class, ATTACHMENT_HEADER + getId());
			} catch (DocumentNotFoundException dne) {
				data = new HashMap<String, Object>();
				data.put("_id", ATTACHMENT_HEADER + getId());
				// dm.getDb().create(data);
			}
			data.put("payload", new String(Base64Coder.encode(cnt)));
			dm.getDb().update(data);
		}
		bDirty = false;
		dm.cdr.update(this);
	}

	public static IOpaqueDocument pull(Document doc) throws ElexisException {
		Document ret = doc;
		if (doc.isProxy) {
			try {
				ret = (Document) doc.dm.getDocument(doc.getId());
				byte[] pidenc = Base64Coder.decode(doc.getPatientID());
				byte[] pidplain = doc.dm.decrypt(pidenc);
				ret.setPatientID(new String(pidplain, "utf-8"));
			} catch (UnsupportedEncodingException e) {
				// should not happen
				e.printStackTrace();
			} catch (CryptologistException e) {
				throw new ElexisException(doc.getClass(), "Cryptology error",
						ElexisException.EE_INTERNAL);
			}
		}
		return ret;
	}

}
