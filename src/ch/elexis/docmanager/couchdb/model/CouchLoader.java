/*******************************************************************************
 * Copyright (c) 2011, G. Weirich and Elexis
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    G. Weirich - initial implementation
 *    
 *    $Id: CouchLoader.java b283431087f6 2011/10/01 17:48:47 rgw $
 *******************************************************************************/

package ch.elexis.docmanager.couchdb.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;

import ch.elexis.ElexisException;
import ch.elexis.text.IOpaqueDocument;

public class CouchLoader extends Job {
	private DocumentManager dm;
	private HashMap<String, List<IOpaqueDocument>> docs =
		new HashMap<String, List<IOpaqueDocument>>();
	private String patID;
	private LoaderCallback lcb;
	
	public CouchLoader(DocumentManager dm){
		super("CouchDB loader");
		this.dm = dm;
		setPriority(LONG);
	}
	
	public void reload(String patID, final LoaderCallback callMeIfFinished){
		if (this.getState() == Job.RUNNING) {
			cancel();
		}
		lcb = callMeIfFinished;
		this.patID = patID;
		schedule();
	}
	
	@Override
	protected IStatus run(IProgressMonitor monitor){
		docs.clear();
		if (patID != null) {
			try {
				for (IOpaqueDocument doc : dm.listDocuments(patID, null, null, null, null, null)) {
					if (monitor.isCanceled()) {
						return Status.CANCEL_STATUS;
					}
					List<IOpaqueDocument> list = docs.get(doc.getCategory());
					if (list == null) {
						list = new ArrayList<IOpaqueDocument>();
						docs.put(doc.getCategory(), list);
					}
					list.add(doc);
				}
				if (lcb != null) {
					lcb.reloaded();
				}
			} catch (ElexisException e) {
				return new Status(Status.ERROR, "ch.elexis.docmanager.couchdb", "could not load: "
					+ e.getMessage());
			}
		}
		return Status.OK_STATUS;
		
	}
	
	public List<IOpaqueDocument> get(String category){
		return docs.get(category);
	}
	
	public String[] getCategories(){
		return docs.keySet().toArray(new String[0]);
	}
	
	public static interface LoaderCallback {
		public void reloaded();
	}
	
}
