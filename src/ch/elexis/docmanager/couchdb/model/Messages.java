package ch.elexis.docmanager.couchdb.model;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "ch.elexis.docmanager.couchdb.model.messages"; //$NON-NLS-1$
	public static String Document_errorRetrievingContents;
	public static String DocumentManager_cantLoadDatabaseDriver;
	public static String DocumentManager_couldNotCreateDatabase;
	public static String DocumentManager_encryptionError;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}
	
	private Messages(){}
}
