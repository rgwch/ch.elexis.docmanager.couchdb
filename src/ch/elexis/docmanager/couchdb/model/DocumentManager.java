/*******************************************************************************
 * Copyright (c) 2011-2012, G. Weirich and Elexis
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    G. Weirich - initial implementation
 *******************************************************************************/

package ch.elexis.docmanager.couchdb.model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.Status;
import org.eclipse.ui.statushandlers.StatusManager;
import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.DocumentNotFoundException;
import org.ektorp.UpdateConflictException;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbInstance;

import ch.elexis.ElexisException;
import ch.elexis.Hub;
import ch.elexis.core.ElexisCoreException;
import ch.elexis.core.Session;
import ch.elexis.docmanager.couchdb.presenter.Facade;
import ch.elexis.services.IDocumentManager;
import ch.elexis.text.IOpaqueDocument;
import ch.rgw.crypt.Base64Coder;
import ch.rgw.crypt.Cryptologist.SYMM_ALGOS;
import ch.rgw.crypt.CryptologistException;
import ch.rgw.crypt.JCECrypter;
import ch.rgw.tools.StringTool;
import ch.rgw.tools.TimeSpan;
import ch.rgw.tools.TimeTool;

public class DocumentManager implements IDocumentManager {
	CouchDocumentRepository cdr;
	CouchDbConnector db;
	String dbid;
	private JCECrypter crypter = new JCECrypter();
	private byte[] key;
	private Document categoryDocument;
	
	/*
	 * private static final String FN_BYPATID = "function(doc) {" //$NON-NLS-1$ +
	 * "if(doc.patientID && doc.title) {" //$NON-NLS-1$ + "	emit(doc.patientID, {title: doc.title,"
	 * + "keywords: doc.keywords," + "patientID: doc.patientID," + "category: doc.category," +
	 * "creationDate: doc.creationDate," + "date: doc.date" + "});}}";
	 * 
	 * private final String FN_ALLDOCS = "function(doc){ if(doc.patientID){" +
	 * " emit(doc.patientID, {title: doc.title," + "keywords: doc.keywords," +
	 * "patientID: doc.patientID," + "category: doc.category," + "creationDate: doc.creationDate," +
	 * "date: doc.date" + " });" + "}}";
	 * 
	 * private static final String FN_BYKEYWORDS = "function(doc) {" //$NON-NLS-1$ +
	 * "if(doc.patientID && doc.keywords) {" //$NON-NLS-1$ +
	 * "	emit(doc.keywords, {title: doc.title," + "keywords: doc.keywords," +
	 * "patientID: doc.patientID," + "category: doc.category," + "creationDate: doc.creationDate," +
	 * "date: doc.date" + "});}}";
	 */
	/*
	 * private static final String FN_BYPATID = "function(doc) {" //$NON-NLS-1$ +
	 * "if(doc.patientID && doc.title) {" //$NON-NLS-1$ +
	 * "	emit(doc.patientID, [{\"category:\" category},{\" keywords: \"keywords}]);" + "}" + "}";
	 */
	private static String catHash;
	
	/**
	 * Open the database connection. If the Database does not exist, it will be created. A
	 * DesignDocument containing a find by PatientID View will be generated
	 * 
	 * @param serverURL
	 *            Address of the server (name or IP, without http://
	 * @param dbNameRaw
	 *            Name of the database. Only lowercase letters allowed
	 * @throws ElexisCoreException
	 */
	public DocumentManager(String serverURL, String dbNameRaw, boolean recreate)
		throws ElexisCoreException{
		try {
			String dbName = dbNameRaw.toLowerCase();
			HttpClient httpClient = new StdHttpClient.Builder().host(serverURL).build();
			CouchDbInstance dbInstance = new StdCouchDbInstance(httpClient);
			if (recreate) {
				dbInstance.deleteDatabase(dbName);
			}
			db = dbInstance.createConnector(dbName, true);
			cdr = new CouchDocumentRepository(db);
			dbid = "xxyc2333"; //$NON-NLS-1$
			
		} catch (RuntimeException re) {
			throw new ElexisCoreException(Messages.DocumentManager_cantLoadDatabaseDriver
				+ re.getMessage());
		}
	}
	
	/**
	 * Open a database connection with the system settings
	 * 
	 * @throws ElexisCoreException
	 */
	public DocumentManager() throws ElexisCoreException{
		this(Facade.getDBUrl(), Facade.getDBName(), false);
		dbid = Session.getInstance().getSystemUID(true);
	}
	
	/**
	 * Retrieve the document that contains the categories of this DocumentManager
	 * 
	 * @return
	 */
	private Document getCategoryDocument(){
		Document doc = categoryDocument;
		if (doc == null) {
			try {
				if (catHash == null) {
					MessageDigest md5 = MessageDigest.getInstance("MD5"); //$NON-NLS-1$
					try {
						md5.update(dbid.getBytes("utf-8")); //$NON-NLS-1$
						md5.update("Categories".getBytes("utf-8")); //$NON-NLS-1$ //$NON-NLS-2$
						byte[] hash = md5.digest();
						catHash = new String(Base64Coder.encode(hash));
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				doc = cdr.get(catHash);
			} catch (DocumentNotFoundException dnfe) {
				doc = new Document(this, "Categories", catHash); //$NON-NLS-1$
				cdr.add(doc);
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				catHash = "123"; //$NON-NLS-1$
			}
			if (doc != null) {
				doc.setDocumentManager(this);
			}
		}
		return doc;
	}
	
	/**
	 * @see IDocumentManager#getCategories
	 */
	
	@Override
	public String[] getCategories(){
		Document doc = getCategoryDocument();
		if (doc == null) {
			return new String[0];
		} else {
			return doc.getKeywords().split(","); //$NON-NLS-1$
		}
	}
	
	/**
	 * @see IDocumentManager#addCategorie
	 */
	@Override
	public boolean addCategory(String categorie){
		Document doc = getCategoryDocument();
		if (doc == null) {
			return false;
		} else {
			List<String> cats = StringTool.splitAL(doc.getKeywords(), ","); //$NON-NLS-1$
			boolean bFound = false;
			for (String cat : cats) {
				if (cat.equals(categorie)) {
					bFound = true;
					break;
				}
				
			}
			if (!bFound) {
				cats.add(categorie);
				doc.setKeywords(StringTool.join(cats, ",")); //$NON-NLS-1$
				try {
					cdr.update(doc);
				} catch (UpdateConflictException uce) {
					// TODO!!
				}
			}
			return true;
		}
	}
	
	public boolean removeCategory(String category, String newCat){
		return false;
	}
	
	public boolean renameCategory(String oldName, String newName){
		return false;
	}
	
	@Override
	public String addDocument(IOpaqueDocument source) throws ElexisException{
		Document doc = new Document(this, source.getTitle(), source.getGUID());
		doc.setKeywords(source.getKeywords());
		doc.setCategory(source.getCategory());
		doc.setPatientID(source.getPatientID());
		try {
			doc.setMimeType(source.getMimeType());
			doc.setContents(source.getContentsAsBytes());
			doc.setCreationDate(new TimeTool(source.getCreationDate()).toString(TimeTool.FULL_ISO));
			// System.out.println(source.getGUID());
			doc.push();
			return doc.getId();
		} catch (UpdateConflictException uce) {
			ElexisException ee =
				new ElexisException(getClass(), "Update conflict",
					ElexisException.EE_INVALID_ARGUMENT);
			StatusManager.getManager().handle(
				new Status(Status.WARNING, Hub.PLUGIN_ID, uce.getMessage(), ee), StatusManager.LOG);
			throw ee;
		} catch (Exception dae) {
			ElexisException ee =
				new ElexisException(getClass(), "DataAccess error: " + dae.getMessage(),
					ElexisException.EE_UNEXPECTED_RESPONSE);
			StatusManager statusManager = StatusManager.getManager();
			statusManager.handle(new Status(Status.ERROR, Hub.PLUGIN_ID, dae.getMessage(), ee),
				StatusManager.LOG);
			throw ee;
		}
		
	}
	
	@Override
	public boolean removeDocument(String guid){
		Document doc = cdr.get(guid);
		cdr.remove(doc);
		return true;
	}
	
	@Override
	public IOpaqueDocument getDocument(String id){
		try {
			Document doc = cdr.get(id);
			doc.setDocumentManager(this);
			return doc;
		} catch (DocumentNotFoundException nf) {
			return null;
		}
		
	}
	
	@Override
	public List<IOpaqueDocument> listDocuments(String patid, String categoryMatch,
		String titleMatch, String keywordMatch, TimeSpan dateMatch, String contentsMatch)
		throws ElexisException{
		/*
		 * ViewResult<Document> vr; if (patid != null) { vr =
		 * db.queryViewByKeys("defaultQueries/byPatientID", Document.class, //$NON-NLS-1$
		 * Arrays.asList(encryptString(patid)), null, null); } else { vr =
		 * db.queryViewByKeys("defaultQueries/byPatientID", Document.class, //$NON-NLS-1$ null,
		 * null, null); } List<ValueRow<Document>> lr = vr.getRows(); List<IOpaqueDocument> ret =
		 * new ArrayList<IOpaqueDocument>(lr.size()); for (ValueRow<Document> row : lr) { //
		 * Document doc = new Document(this, row.getValue(), row.getId()); Document doc =
		 * row.getValue(); doc.setDocumentManager(this); doc.setId(row.getId()); ret.add(doc); }
		 * return ret;
		 */
		List<Document> raw = cdr.findByPatientID(encryptString(patid));
		List<IOpaqueDocument> ret = new ArrayList<IOpaqueDocument>(raw.size());
		for (Document doc : raw) {
			doc.setDocumentManager(this);
			ret.add(doc);
		}
		return ret;
	}
	
	public Document createDocumentInstance(){
		Document doc = new Document(this);
		doc.setCreationDate(new TimeTool().toString(TimeTool.FULL_ISO));
		return doc;
	}
	
	String encryptString(String string) throws ElexisException{
		try {
			byte[] pidplain = string.getBytes("utf-8");
			byte[] pidenc = encrypt(pidplain);
			return new String(Base64Coder.encode(pidenc));
		} catch (UpdateConflictException uce) {
			throw new ElexisException(getClass(), "Update conflict",
				ElexisException.EE_UPDATE_ERROR);
		} catch (UnsupportedEncodingException e) {
			// should not happen
			e.printStackTrace();
			return null;
		} catch (CryptologistException e) {
			throw new ElexisException(getClass(), "Cryptology error", ElexisException.EE_INTERNAL);
		}
	}
	
	byte[] encrypt(byte[] plain) throws CryptologistException{
		if (key == null) {
			String k = Facade.getKey();
			key = Base64Coder.decode(k);
		}
		ByteArrayInputStream bais = new ByteArrayInputStream(plain);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		crypter.encrypt(bais, baos, SYMM_ALGOS.BLOWFISH, key);
		return baos.toByteArray();
	}
	
	byte[] decrypt(byte[] encrypted) throws CryptologistException{
		if (key == null) {
			String k = Facade.getKey();
			key = Base64Coder.decode(k);
		}
		ByteArrayInputStream bais = new ByteArrayInputStream(encrypted);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		crypter.decrypt(bais, baos, key);
		return baos.toByteArray();
	}
	
	byte[] decrypt(InputStream encrypted) throws CryptologistException{
		if (key == null) {
			String k = Facade.getKey();
			key = Base64Coder.decode(k);
		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		crypter.decrypt(encrypted, baos, key);
		return baos.toByteArray();
		
	}
	
	public CouchDbConnector getDb(){
		return db;
	}
}
