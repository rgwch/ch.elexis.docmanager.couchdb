h1. Elexis Dokumentenmanagement "CouchDB"

h2. Worum geht es?

In Elexis kann die Dokumentenverwaltung von unterschiedlichen Plugins übernommen werden. Es ist so möglich, je nach Vorlieben, Bedürfnissen, oder auch nach Dokumenttypen unterschiedliche Dokumentenverwaltungssysteme einzusetzen. Nebst den schon länger bekannten Systemen Omnivore, Omnivore plus und Omnivore direct erhalten Sie mit CouchDB nun ein Dokumentenmanagement, das auf einer völlig anderen Basis aufbaut (aber gleichwohl natürlich nahtlos in Elexis integriert ist)

h3. CouchDB

CouchDB ist ein "halbironisches" Akronym, welches für  „Cluster of unreliable commodity hardware Data Base“ steht, übersetzt also so ungefähr "Datenbank auf einer Ansammlung unzuverlässiger Standardhardware". Gleichzeitig soll die "Couch" symbolisieren, dass man sich entspannen kann, wenn CouchDB die Daten übernimmt. CouchDB wird seit 2005 von der Apache Software Foundation entwickelt. 

h3. Zuverlässigkeit

CouchDB ist von vornherein darauf ausgelegt, dass Computer und Festplatten kaputtgehen können. Sie hat daher ein fest eingebautes Replikationskonzept: Jedes Dokument kann auf beliebig viele Computer an beliebigen Orten der Welt verteilt werden. Man muss das nur einmal einrichten, dann läuft es automatisch ab. Es ist also kein explizites Backup mehr notwendig. Und im Gegensatz zu einem normalen Inhouse-Backup übersteht eine CouchDB Datenbank auch grössere Katastrophen, weil sie geographisch verteilt sein kann.

h3. Skalierbarkeit

Wenn die Datenmenge so gross wird, dass ein Server nicht mehr reicht, oder wenn soviele Zugriffe erfolgen, dass ein Server die Last nicht mehr schnell genug abarbeiten kann, dann lässt sich die CouchDB problemlos auf mehrere Server aufteilen. Ein Zusatzprogramm namens "Lounge" übernimmt die Verteilung der Anfragen auf die richtigen Server. Und jeder dieser Server kann per Replikation wieder redundant angelegt werden.


h2. Warum in Elexis?

Wir haben speziell in Gruppenpraxen gesehen, dass das bisherige Dokumentenmanagement manchmal an Grenzen stösst. In Praxen, die sehr viele Dokumente einscannen, entstehen innert weniger Jahre Datenbestände vpon -zig Gigabyte, wo der Zugriff über den Elexis-Server dann langsamer werden kann, und wo vor allem das Backup technisch und organisatorisch viel schwieriger wird (Grosse Datenmengen müssen zeitaufwändig auf ein externes Medium übertragen werden, was die normale Arbeit verlangsamt)

Ein anderer Grund ist folgender: Mit der zunehmenden Bedeutung von Ärztenetzwerken wächst auch das Bedürfnis nach Dokumenten, die nicht nur einer einzelnen Praxis zur Verfügung stehen. Für solche Zwecke ist die bisherige Elexis-Datenbank überhaupt nicht geeignet.

h3. Und der Datenschutz?

Alle Dokumente werden verschlüsselt gespeichert und können nur von denjenigen gelesen werden, die im Besitz eines gültigen Schlüssels sind.

h2. Installation und Konfiguration

h3. CouchDB

Es gibt viele Möglichkeiten, CouchDB zu installieren und zu nutzen. Sie können CouchDB auf Ihrem vorhandenen Server einrichten oder einen separaten Server speziell für die Dokumentenverwaltung einrichten, oder aber Sie könnten Serverkapazität im Internet oder bei einem bestimmten Provider mieten.

Die Installation und Konfiguration des CouchDB Servers ist daher nicht Thema dieses Dokuments. Lassen Sie sich bitte ggf. beraten.

h3. Client Plugin

Installieren Sie in Elexis das Plugin CouchDB-Dokumente wie gewohnt manuell oder über den Konfigurator. Starten Sie Elexis neu und geben Sie unter Datei-Einstellungen-Dokumentenverwaltung-CouchDB die nötigern Angaben ein:

!couchdb0.png!

* Server URL: Die Adresse des Servers. Dies kann eine IP oder ein symbolischer name (wie couchdb.myserver.ch) sein.
* Datenbankname: Irgendein Wort ohne Sonderzeichen oder Umlaute. Wenn eine Datenbank dieses Namens auf dem angegebenen Server nicht existiert, wird sie neu erstellt.
* Verschlüsselt speichern: Dies sollten Sie anwählen, wenn die Datenbank sich ausserhalb Ihrer unmittelbaren Kontrolle befindet (z.B. auf einem Server im Internet). In diesem Fall werden alle Dokumente verschlüsselt abgelegt. Der Schlüssel wird beim ersten Start automatisch erstellt und in der Elexis-Datenbank hinterlegt. Wenn Sie die Datenbank und damit den Schlüssel verlieren, gibt es keine Möglichkeit, die verschlüsselt abgelegten Dokumente wieder zurückzubekommen. Wenn der Server und sämtliche Replikationen der Datenbank sich in Ihren Räumlichkeiten befindet, können Sie auf Verschlüsselung verzichten. Der Zugriff erfolgt dann etwas schneller, aber der Unterschied wird in den meisten Fällen kaum messbar sein.

h2. Transfer existierender Dokumente

Vermutlich haben Sie bisher schon mit einem anderen Dokumentmanagement-System gearbeitet und möchten die Dokumente ins neue System übernehmen. Dies ist kein Problem: Klicken Sie in Datei-Einstellungen-Dokumentenverwaltung auf den Knopf "Dokumente neu verteilen" (oder "Redistribute documents"). Es wird dann ein Prozess gestartet, welcher im Hintergrund alle Dokumente aus anderen Dokumentenmanagern ausliest und in CouchDB abspeichert. Dieser Prozess kann je nach Datenmenge mehrere Stunden dauern. Sie erkennen am Fortschrittsbalken rechts unten im Elexis-Fenster, dass er noch aktiv ist. Sie können unterdessen weiterarbeiten, aber Sie sollten Elexis nicht beenden, bevor der Prozess abgeschlossen ist. Sie sollten den Prozess auch keinesfalls auf mehreren Arbeitsstationen gleichzeitig starten.

!couchdb1.png!

h2. CouchDb View

Nach erfolgreicher Installation und Konfiguration sind die Dokumente über die CouchDB View zugänglich:

!couchdb5.png!

Import und Export der Dokomente erfolgt gleich wie beim Omnivore-System


h2. Technisches

Dieser Abschnitt ist nut für näher Interessierte gedacht und für die Benutzung des Plugins nicht notwendig.

h3. Web-Access

Eine CouchDB-Datenbank kann über einen gewöhnlichen Web-Browser eingesehen, manipuliert und konfiguriert werden:

!couchdb2.png!

Sie sehen hier die gespeicherten Dokumente als Liste von Id's (die per se nicht sehr aussagekräftig sind). Klick auf eine ID öffnet die Meta-Informationen des Dokuments.

!couchdb3.png!

Der eigentliche Inhalt des Dokuments ist unter "contents" gespeichert. In diesem Fall hier, wo der Inhalt nicht verschlüsselt abgespeichert wurde, kann man mit Klick auf contents auch gleich das Dokument im Browser lesen.

h3. Web-Administration

Durch Klick auf "Replicator" im web-Frontend kann man zum Beispiel die automatsiche Replikation einschalten (Achtung: Wenn die Replikation auf einen öffentlich zugänglichen Server geht, muss die Speicherung verschlüsselt erfolgen)

!couchdb4.png!

